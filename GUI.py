import wx
import subprocess
import psutil
import os
import json
import stat
from wx.lib.softwareupdate import SoftwareUpdate
App_Dir=os.getcwd()
CheckBoxesList=[]
ModListLeft=[]
Arma3Directory=""
MissionsDirectory=""
parameters={}
CPU=1
RAM=256
GPU=128
THREADS=0
priorityList=[]
profiles=[]
ModsListBox=wx.ListBox
EditorMissionsListBox=wx.ListBox
MissionsFolderChoices=[]
MissionsList=[]
RamSlider=wx.Slider
GpuSlider=wx.Slider
PcuCombo=wx.ComboBox
ThreadsCombo=wx.ComboBox
RamSliderTextCtrl=wx.TextCtrl
GpuSliderTextCtrl=wx.TextCtrl
GeometryLoadingYES=wx.StaticBitmap
GeometryLoadingNO=wx.StaticBitmap
FileOperationsYES=wx.StaticBitmap
FileOperationsNO=wx.StaticBitmap
TextureLoadingYES=wx.StaticBitmap
TextureLoadingNO=wx.StaticBitmap
exThreads=[]
cores=[]
selectAllButton=wx.ToggleButton
ProfileBox=wx.ComboBox
favouriteprofile=""

class PageOne(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		global priorityList,ModsListBox,Arma3Directory,selectAllButton

		check(self)
		LoadProfiles(self)
		LoadSettings(self)

		RunButton=wx.Button(self,label="Run ARMA 3",pos=(250,450),size=(100,30))
		
		ModsListBox=wx.ListBox(self, -1, (200,20),(200, 300), priorityList, wx.LB_SINGLE)
		
		MoveFirstPNG=wx.Image("images/hide-top.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap()
		MoveUpPNG=wx.Image("images/navigate-up.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap()
		MoveDownPNG=wx.Image("images/navigate-down.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap()

		wx.BitmapButton(self, -1, MoveFirstPNG, (450, 10), (80,80),name="MoveFirst")
		wx.BitmapButton(self, -1, MoveUpPNG, (450, 110),(80,80),name="MoveUp")
		wx.BitmapButton(self, -1, MoveDownPNG, (450, 210), (80,80),name="MoveDown")

		self.Bind(wx.EVT_BUTTON, self.UpDown)

		selectAllButton=wx.ToggleButton(self, -1, 'Select All', (30, 450), (80,30))
		self.Bind(wx.EVT_TOGGLEBUTTON, self.SelectAllMods,selectAllButton)
		selectAllButton.SetValue(False)
		if len(ModListLeft)==len(priorityList):
			selectAllButton.SetValue(True)
			selectAllButton.SetLabel("Deselect All")

		self.Bind(wx.EVT_CHECKBOX,OnChecked)
		self.Bind(wx.EVT_BUTTON, self.runArma3, RunButton)

	def runArma3(self,event):
		global parameters,Arma3Directory
		if not Arma3Directory:message()
		else:
			counter=0
			par = []
			for i,j in parameters.items():
				if isinstance(j,bool):
					par.insert(0,i)
				else:
					par.insert(0,i+str(j))
			if priorityList==[]:pass
			else:
				stringOfMods=""
				stringOfMods=';'.join(priorityList)
				par.append("-mod="+stringOfMods)

			subprocess.Popen([Arma3Directory+"\\arma3.exe"]+par)	

	def SelectAllMods(self,event):
		global ModListLeft,priorityList,ModsListBox
		sender=event.GetEventObject()
		if sender.GetValue()==False:
			for objects in ModListLeft:
				objects.SetValue(False)
				del priorityList[:]
			sender.SetLabel("Select All")

		elif sender.GetValue()==True:
			for objects in ModListLeft:
				if objects.GetValue()==False:
					objects.SetValue(True)
					priorityList.append(objects.GetName())
				else:pass
				
			sender.SetLabel("Deselect All")

		ModsListBox.Set(priorityList)

	def UpDown(self,event):
		global ModsListBox,priorityList
		sender=event.GetEventObject()

		if ModsListBox.GetStringSelection()=="" or len(priorityList)<2:
			pass
		else:
			SelectedMod=ModsListBox.GetStringSelection()
			ModPosition=priorityList.index(SelectedMod)
			selectedPosition=ModPosition
			if sender.GetName()=="MoveUp" and ModPosition>0:
				priorityList[ModPosition],priorityList[ModPosition-1]=priorityList[ModPosition-1],priorityList[ModPosition]
				selectedPosition=ModPosition-1
			elif sender.GetName()=="MoveDown" and ModPosition<len(priorityList)-1:
				priorityList[ModPosition],priorityList[ModPosition+1]=priorityList[ModPosition+1],priorityList[ModPosition]
				selectedPosition=ModPosition+1
			elif sender.GetName()=="MoveFirst":
				priorityList.remove(SelectedMod)
				priorityList.insert(0,SelectedMod)
				selectedPosition=0

			ModsListBox.Set(priorityList)
			ModsListBox.SetSelection(selectedPosition)


def YesNoImage():
	if int(THREADS)>0 and CheckBoxesList[13].GetValue()==True:
		FileOperationsNO.Hide()
		FileOperationsYES.Show()
		if int(THREADS)==3 or int(THREADS)==7:
			TextureLoadingNO.Hide()
			TextureLoadingYES.Show()
		else:
			TextureLoadingNO.Show()
			TextureLoadingYES.Hide()
		if int(THREADS)==5 or int(THREADS)==7:
			GeometryLoadingYES.Show()
			GeometryLoadingNO.Hide()
		else:
			GeometryLoadingYES.Hide()
			GeometryLoadingNO.Show()
	else:
		FileOperationsNO.Show()
		FileOperationsYES.Hide()
		TextureLoadingNO.Show()
		TextureLoadingYES.Hide()
		GeometryLoadingYES.Hide()
		GeometryLoadingNO.Show()
#------------------------------------------------------------------------------------------------------------
#----------------------------------------------------TAB 2-------------------------------------------------
#------------------------------------------------------------------------------------------------------------

class PageTwo(wx.Panel):
	def __init__(self, parent):

		wx.Panel.__init__(self, parent)
		global RamSlider,GpuSlider,PcuCombo,ThreadsCombo,RamSliderTextCtrl,GpuSliderTextCtrl,RAM,GPU,THREADS,GeometryLoadingYES,GeometryLoadingNO,FileOperationsYES,FileOperationsNO,TextureLoadingYES,TextureLoadingNO,cores,exThreads
		
		YesImage=wx.Image("images/apply.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
		NoImage=wx.Image("images/delete.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
		FileOperationsYES=wx.StaticBitmap(self, -1, YesImage, (330, 280), (YesImage.GetWidth(), YesImage.GetHeight()))
		FileOperationsNO=wx.StaticBitmap(self, -1, NoImage, (330, 280), (NoImage.GetWidth(), NoImage.GetHeight()))
		TextureLoadingYES=wx.StaticBitmap(self, -1, YesImage, (330, 300), (YesImage.GetWidth(), YesImage.GetHeight()))
		TextureLoadingNO=wx.StaticBitmap(self, -1, NoImage, (330, 300), (NoImage.GetWidth(), NoImage.GetHeight()))
		GeometryLoadingYES=wx.StaticBitmap(self, -1, YesImage, (330, 320), (YesImage.GetWidth(), YesImage.GetHeight()))
		GeometryLoadingNO=wx.StaticBitmap(self, -1, NoImage, (330, 320), (NoImage.GetWidth(), NoImage.GetHeight()))
		GeometryLoadingYES.Hide()
		GeometryLoadingNO.Hide()
		FileOperationsYES.Hide()
		FileOperationsNO.Hide()
		TextureLoadingYES.Hide()
		TextureLoadingNO.Hide()

		self.SetBackgroundColour('#aaaaaa')

		wx.StaticBox(self, -1, 'Basic Parameters', (20, 20),(200,330))

		CheckBoxesList.extend((
			wx.CheckBox(self, -1, "Empty World", (30,45),name='-world=empty'),
			wx.CheckBox(self, -1, "No Logs", (30,75),name='-noLogs'),
			wx.CheckBox(self, -1, "Show Script Errors", (30,105),name='-showScriptErrors'),
			wx.CheckBox(self, -1, "Run in window", (30,135),name='-window'),
			wx.CheckBox(self, -1, "Skip Intro", (30,165),name='-skipIntro'),
			wx.CheckBox(self, -1, "Run with Direct3D only", (30,195),name='-winxp'),
			wx.CheckBox(self, -1, "Turn off multicore", (30,225),name='-noCB'),
			wx.CheckBox(self, -1, "Enable running in background", (30,255),name='-noPause'),
			wx.CheckBox(self, -1, "Load only PBO's", (30,285),name='-noFilePatching'),
			wx.CheckBox(self, -1, "Disable Splash Screen", (30,315),name='-nosplash'),
			wx.CheckBox(self, -1, "CPU cores", (260,45),name='-cpuCount='),
			wx.CheckBox(self, -1, "Ram", (260,90), name='-maxMem='),
			wx.CheckBox(self, -1, "GPU Memory", (260,160),name='-maxVRAM='),
			wx.CheckBox(self, -1, "Extra threads", (260,230),name='-exThreads=')
			))
		
		wx.StaticBox(self, -1, 'Advanced Parameters',(250, 20),(320,330))

		for i in range(1,psutil.NUM_CPUS+1):
			cores.append(str(i))
		PcuCombo=wx.ComboBox(self, -1,value="1",pos=(370, 41), choices=cores, style=wx.CB_READONLY)
		RamSliderTextCtrl=wx.TextCtrl(self,pos=(364,85),size=(50,-1),style=wx.TE_READONLY)
		RamSliderTextCtrl.SetValue(str(RAM))
		RamSliderTextCtrl.Disable()
		wx.StaticText(self, -1, "MB", (420, 90))
		RamSlider=wx.Slider(self, -1, 1, 256, psutil.virtual_memory()[0]/1024**2, pos=(315, 110),size=(150, -1),style=wx.SL_HORIZONTAL)
		GpuSliderTextCtrl=wx.TextCtrl(self,pos=(364,155),size=(50,-1),style=wx.TE_READONLY)
		GpuSliderTextCtrl.SetValue(str(GPU))
		GpuSliderTextCtrl.Disable()
		wx.StaticText(self, -1, "MB", (420, 160))
		GpuSlider=wx.Slider(self, -1, 1, 128, 2047, pos=(315, 180),size=(150, -1),style=wx.SL_HORIZONTAL)
		exThreads = ["0","1", "3", "5", "7"]
		ThreadsCombo=wx.ComboBox(self, -1,value="0", pos=(370, 225), choices=exThreads, style=wx.CB_READONLY)
		CheckBoxesList.extend((PcuCombo,RamSlider,GpuSlider,ThreadsCombo))

		wx.StaticText(self, -1, "File Operations", (350, 280))
		wx.StaticText(self, -1, "Texture Loading", (350, 300))
		wx.StaticText(self, -1, "Geometry Loading", (350, 320))

		for objects in CheckBoxesList[14:]:
			objects.Disable()

		for i,j in parameters.items():
			for objects in CheckBoxesList:
				if i==objects.GetName():
					objects.SetValue(True)
				elif i=="-cpuCount=":
					PcuCombo.Enable()
					PcuCombo.SetSelection(cores.index(str(j)))
					CPU=j
				elif i=="-exThreads=":
					ThreadsCombo.Enable()
					ThreadsCombo.SetSelection(exThreads.index(str(j)))
					THREADS=j
				elif i=="-maxMem=":
					RamSlider.Enable()
					RamSliderTextCtrl.Enable()
					RamSlider.SetValue(j)
					RamSliderTextCtrl.SetValue(str(j))
					RAM=j
				elif i=="-maxVRAM=":
					GpuSlider.Enable()
					GpuSliderTextCtrl.Enable()
					GpuSlider.SetValue(j)
					GpuSliderTextCtrl.SetValue(str(j))
					GPU=j

		
		self.Bind(wx.EVT_COMBOBOX,self.OnSelect)
		self.Bind(wx.EVT_SLIDER,self.OnAdjust)
		self.Bind(wx.EVT_CHECKBOX,OnChecked)		
		YesNoImage()

	def OnSelect(self,event):
		global parameters,CPU,THREADS
		sender=event.GetEventObject()
		if sender==PcuCombo:
			if "-cpuCount=" in parameters:parameters["-cpuCount="]=sender.GetValue()
			CPU=PcuCombo.GetValue()
		elif sender==ThreadsCombo:
			if "-exThreads=" in parameters:parameters["-exThreads="]=sender.GetValue()
			THREADS=sender.GetValue()
			YesNoImage()

	def OnAdjust(self,event):
		global parameters,RAM,GPU,RamSliderTextCtrl,GpuSliderTextCtrl
		sender=event.GetEventObject()
		if sender==RamSlider:
			if "-maxMem=" in parameters:parameters["-maxMem="]=sender.GetValue()
			RAM=sender.GetValue()
			RamSliderTextCtrl.SetValue(str(sender.GetValue()))
		elif sender==GpuSlider:
			if "-maxVRAM=" in parameters:parameters["-maxVRAM="]=sender.GetValue()
			GPU=sender.GetValue()
			GpuSliderTextCtrl.SetValue(str(sender.GetValue()))

	
#------------------------------------------------------------------------------------------------------------
#----------------------------------------------------TAB 3-------------------------------------------------
#------------------------------------------------------------------------------------------------------------
class PageThree(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		global MissionsDirectory,EditorMissionsListBox,MissionsFolderChoices
		EditorMissionsListBox=wx.ListBox(self, -1, (200,20),(200, 300), MissionsFolderChoices, wx.LB_SINGLE)
		if not MissionsDirectory:pass
		else:
			MissionsFolderChoices=os.listdir(MissionsDirectory)
			self.Selection=""
			self.Bind(wx.EVT_LISTBOX,self.OnSelectedMission,EditorMissionsListBox)
			self.Bind(wx.EVT_LISTBOX_DCLICK, self.openMissionFolder, EditorMissionsListBox)

		RunButton=wx.Button(self,label="Run ARMA 3 Editor",pos=(250,450),size=(100,30))
		self.Bind(wx.EVT_BUTTON, self.runArma3EDITOR, RunButton)
		
	def OnSelectedMission(self,event):
		global MissionsList,MissionsDirectory
		pointer=event.GetSelection()
		if MissionsFolderChoices[pointer]==self.Selection:pass
		else:
			del MissionsList[:]
			MissionsList.append(MissionsDirectory+"\\"+MissionsFolderChoices[pointer]+"\\mission.sqm")
			self.Selection=MissionsFolderChoices[pointer]

	def openMissionFolder(self,event):
		global MissionsDirectory
		subprocess.Popen(r'explorer ' + MissionsDirectory+"\\"+self.Selection)

	def runArma3EDITOR(self,event):
		global Arma3Directory,MissionsList,parameters
		if not Arma3Directory:
			message()
		elif len(MissionsList)==0:
			dlg = wx.MessageDialog(self, 'Select Arma 3 directory in settings first!', 'Oups!', wx.OK|wx.ICON_INFORMATION)
			dlg.ShowModal()
			dlg.Destroy()
		else:
			counter=0
			par = []
			for i,j in parameters.items():
				if isinstance(j,bool):
					par.insert(0,i)
				else:
					par.insert(0,i+str(j))
			if priorityList==[]:pass
			else:
				stringOfMods=""
				stringOfMods=';'.join(priorityList)
				par.append("-mod="+stringOfMods)
			par.append(MissionsList[0])
			subprocess.Popen([Arma3Directory+"\\arma3.exe"]+par)
				


class PageFour(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		

#---------------------------------------------------------------------------------------------------------------------
#--------------------------------------------GLOBALFUNCTIONS-------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------


def check(self):
	global Arma3Directory,MissionsDirectory
	try:
		with open("config.txt",'r') as fob:
			temp=json.load(fob)
			if len(temp)==0:pass
			else:
				if len(temp)==1:
					Arma3Directory=temp[0]
				else:
					Arma3Directory=temp[0]
					MissionsDirectory=temp[1]

				folderList=os.listdir(Arma3Directory)
				i=20
				for modName in folderList:
					if "@" in modName and os.path.isdir(Arma3Directory+"\\"+modName+"\\addons")==True:
						ModListLeft.append(wx.CheckBox(self, -1, modName, (20,i), (-1,-1),name=modName))
						i +=25
	except:None
	
	
	# with open("config.txt",'r') as f:
	# 		temp=json.load(f)
	# ProfileBox.SetSelection(len(profiles)-1)
def LoadSettings(self):
	global parameters,priorityList,ModListLeft,Arma3Directory,profiles,ProfileBox,favouriteprofile

	if not Arma3Directory:pass
	else:
		try:
			with open(App_Dir+"\\config.txt") as fob:
				temp=json.load(fob)
			favouriteprofile=temp[2]
			with open(App_Dir+"\\Profiles\\"+favouriteprofile+".txt") as fob:
				settings=json.load(fob)
				parameters=settings[0]
				priorityList=settings[1]

			
		except:None

	for objects in ModListLeft:
		if objects.GetName() in priorityList:
			objects.SetValue(True)
	#ProfileBox.SetSelection("a")
	#self.Bind(wx.EVT_CHECKBOX,OnChecked)
def LoadProfiles(self):
	global ProfileBox
	if not Arma3Directory:
		pass
	else:
		dire=App_Dir+"\Profiles"
		folderlist=os.listdir(dire)
		for profile in folderlist:
			profiles.append(str(profile).replace(".txt",""))

def message():
	dlg = wx.MessageDialog(None, 'Select Arma 3 directory in settings first!', 'Oups!', wx.OK|wx.ICON_INFORMATION)
	dlg.ShowModal()
	dlg.Destroy()
def OpenArma3Folder(event):
	global Arma3Directory
	if not Arma3Directory:
		message()
	else:
		subprocess.Popen(r'explorer '+Arma3Directory)

def OnChecked(event):
		global parameters,CPU,RAM,GPU,THREADS,priorityList,ModsListBox,RamSlider,GpuSlider,PcuCombo,ThreadsCombo,RamSliderTextCtrl,GpuSliderTextCtrl
		sender=event.GetEventObject()

		if sender.GetValue()==True:
			if sender.GetName().startswith("@"):
				priorityList.append(sender.GetName())
			elif sender.GetName()=="-cpuCount=":
				PcuCombo.Enable()
				parameters.update({"-cpuCount=":PcuCombo.GetValue()})
			elif sender.GetName()=="-maxMem=":
				RamSlider.Enable()
				RamSliderTextCtrl.Enable()
				RamSliderTextCtrl.SetValue(str(RamSlider.GetValue()))
				parameters.update({"-maxMem=":RamSlider.GetValue()})
			elif sender.GetName()=="-maxVRAM=":
				GpuSlider.Enable()
				GpuSliderTextCtrl.Enable()
				GpuSliderTextCtrl.SetValue(str(GpuSlider.GetValue()))
				parameters.update({"-maxVRAM=":GpuSlider.GetValue()})
			elif sender.GetName()=="-exThreads=":
				ThreadsCombo.Enable()
				parameters.update({"-exThreads=":ThreadsCombo.GetValue()})
				YesNoImage()
			else:
				parameters.update({str(sender.GetName()): True})
		else:
			if sender.GetName().startswith("@"):
				priorityList.remove(sender.GetName())
			else:
				if sender.GetName()=="-cpuCount=":
					PcuCombo.Disable()
				elif sender.GetName()=="-maxMem=":
					RamSlider.Disable()
					RamSliderTextCtrl.Disable()
				elif sender.GetName()=="-maxVRAM=":
					GpuSlider.Disable()
					GpuSliderTextCtrl.Disable()
				elif sender.GetName()=="-exThreads=":
					ThreadsCombo.Disable()
					YesNoImage()
				elif sender.GetName().startswith("@"):
					priorityList.remove(sender.GetName())
				del parameters[str(sender.GetName())]
				 
		ModsListBox.Set(priorityList)

#----------------------------------------------------------------------------------------------
#--------------------------------------------MAIN----------------------------------------------
#----------------------------------------------------------------------------------------------
class MainFrame(wx.Frame):

	def __init__(self,parent,id):
#-------Start of actual program
		no_resize =wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CLOSE_BOX | wx.CLIP_CHILDREN
		
		wx.Frame.__init__(self,parent,id,'HACv Launcher', size=(1000,800),style=no_resize)
		global profiles,ProfileBox,favouriteprofile

		self.tskic = MyTaskBarIcon(self)
		self.Centre()
		self.Bind(wx.EVT_CLOSE, self.OnClose)
		status=self.CreateStatusBar()   
#-------Tabs        
		self.nb=wx.Notebook(self, id=-1, style=wx.BK_DEFAULT)

		self.page1=PageOne(self.nb)
		self.page2=PageTwo(self.nb)
		self.page3=PageThree(self.nb)
		self.page4=PageFour(self.nb)

		self.nb.AddPage(self.page1,"Mods")
		self.nb.AddPage(self.page2,"Parametres")
		self.nb.AddPage(self.page3,"Editor")
		self.nb.AddPage(self.page4,"HACv Mod")

		sizer = wx.BoxSizer()
		sizer.Add(self.nb, 1, wx.EXPAND)
		self.SetSizer(sizer)

#-------Setting up the Menu
		fileMenu=wx.Menu()
		fileMenu.AppendSeparator()
		

		settingsMenu=wx.Menu()
		folders=settingsMenu.Append(-1,"Choose Arma 3 folder","Specify Arma 3 folder")
		missions=settingsMenu.Append(-1,"Choose Missions folder","Specify documents missions folder")

		helpMenu=wx.Menu()
		about=helpMenu.Append(-1,"About","About the program")
		
#------Create Menu
		menubar=wx.MenuBar()
		menubar.Append(fileMenu,"File")
		menubar.Append(settingsMenu,"Settings")
		menubar.Append(helpMenu,"Help")
		self.SetMenuBar(menubar)
		

		self.Bind(wx.EVT_MENU,self.OnFolders,folders)
		self.Bind(wx.EVT_MENU,self.OnMissions,missions)
		self.Bind(wx.EVT_MENU,self.OnAbout,about)
#-------TOOLBAR
		self.toolbar=self.CreateToolBar(style=wx.TB_NODIVIDER)
		Refresh=self.toolbar.AddLabelTool(-1, 'Refresh',wx.Bitmap('images/refresh.png'))
		self.Bind(wx.EVT_TOOL, self.RefreshMods, Refresh)
		self.toolbar.AddSeparator()
		OpenFolderA3=self.toolbar.AddLabelTool(-1, 'Open Arma 3 folder',wx.Bitmap('images/file-explorer.png'))
		self.Bind(wx.EVT_TOOL, OpenArma3Folder, OpenFolderA3)
		self.toolbar.AddSeparator()

		ProfileBox=wx.ComboBox(self.toolbar,-1,value="default", choices=profiles, style=wx.CB_READONLY)
		self.toolbar.AddControl(ProfileBox, 'ProfileBox')
		self.Bind(wx.EVT_COMBOBOX, self.OnSelectProfile, ProfileBox)
		self.toolbar.AddSeparator()
		Save=self.toolbar.AddLabelTool(5, 'Save Settings',wx.Bitmap('images/save.png'))
		self.Bind(wx.EVT_TOOL, self.SaveSettings, Save)
		self.toolbar.AddSeparator()
		NewProfile=self.toolbar.AddLabelTool(-1, 'Create profile',wx.Bitmap('images/newprofile.png'))
		self.Bind(wx.EVT_TOOL, self.CreateProfile, NewProfile)
		self.toolbar.AddSeparator()
		DeleteProfile=self.toolbar.AddLabelTool(3, 'Delete profile',wx.Bitmap('images/deleteprofile.png'))
		if favouriteprofile=="" and os.path.exists(App_Dir+"\\Profiles\\"+favouriteprofile+".txt"):
			ProfileBox.SetSelection(favouriteprofile)

		if ProfileBox.GetSelection()==-1:
			self.toolbar.EnableTool(3,False)
			self.toolbar.EnableTool(5,False)
			
		self.Bind(wx.EVT_TOOL, self.OnDeleteProfile, DeleteProfile)
		self.toolbar.AddSeparator()
		self.NewProfileText = wx.TextCtrl(self.toolbar, -1, "",pos=wx.Point(650, 12))
		FavProfile=self.toolbar.AddLabelTool(4, 'Favorite profile',wx.Bitmap('images/favorite.png'))
		self.Bind(wx.EVT_TOOL, self.FavoriteProfile, FavProfile)
		self.toolbar.EnableTool(4,False)
		self.toolbar.AddSeparator()
		Update=self.toolbar.AddLabelTool(-1, 'Update',wx.Bitmap('images/refresh.png'))
		self.Bind(wx.EVT_TOOL, self.OnUpdate, Update)
		self.toolbar.AddSeparator()
		Settings=self.toolbar.AddLabelTool(-1, 'Settings',wx.Bitmap('images/settings.png'))
		self.Bind(wx.EVT_TOOL, self.OnSettings, Settings)
		self.toolbar.AddSeparator()
		Exit=self.toolbar.AddLabelTool(-1, 'Exit',wx.Bitmap('images/exit.png'))
		self.Bind(wx.EVT_TOOL, self.OnExit, Exit)

		self.toolbar.AddSeparator()

		self.toolbar.Realize()
		self.toolbar.SetSize((250,200))
		self.toolbar.Centre()
		self.toolbar.Show(True)

#-------ToolBar Functions----------------------------------------------------------------------
	def OnSettings(self,event):
				pass
		
	def OnUpdate(self,event):
		wx.GetApp().CheckForUpdate(parentWindow=self)

	def FavoriteProfile(self,event):
		global favouriteprofile
		favouriteprofile=ProfileBox.GetStringSelection()
		
		try:
			with open("config.txt",'r') as f:
				temp=json.load(f)
			if len(temp)>=3:
				temp[2]=favouriteprofile
			elif len(temp)==2:
				temp.append(favouriteprofile)
			elif len(temp)==1:
				temp.append("")
				temp.append(favouriteprofile)
			else:
				temp.append("")
				temp.append("")
				temp.append(favouriteprofile)
			os.chmod(App_Dir+"\\config.txt",stat.S_IWRITE)
			f=open("config.txt",'w')
			json.dump(temp,f)
			f.close()
		except :
			temp=[]
			temp.append("")
			temp.append("")
			temp.append(favouriteprofile)
			os.chmod(App_Dir+"\\config.txt",stat.S_IWRITE)
			with open("config.txt",'w') as f:
				json.dump(temp,f)
		os.chmod(App_Dir+"\\config.txt",stat.S_IREAD)
		self.toolbar.EnableTool(4,False)

	def OnDeleteProfile(self,event):
		global favouriteprofile
		profileForDelete=ProfileBox.GetStringSelection()
		dial = wx.MessageDialog(None, 'Are you sure yu want to delete the profile ?', 'Info', wx.CANCEL| wx.CANCEL_DEFAULT|wx.ICON_QUESTION)
		if dial.ShowModal() == wx.ID_OK:
			if profileForDelete==favouriteprofile and os.path.exists(App_Dir+"\\Profiles\\"+favouriteprofile+".txt"):
				favouriteprofile==""
				temp=[Arma3Directory,MissionsDirectory]
				os.chmod(App_Dir+"\\config.txt",stat.S_IWRITE)
				with open("config.txt",'w') as f:
					json.dump(temp,f)
			os.chmod(App_Dir+"\\config.txt",stat.S_IREAD)
			os.chmod(App_Dir+"\\Profiles/"+profileForDelete+".txt",stat.S_IWRITE)
			os.remove(App_Dir+"\\Profiles\\"+profileForDelete+".txt")
			profiles.remove(profileForDelete)
			ProfileBox.Set(profiles)
			self.Bind(wx.EVT_CHECKBOX,OnChecked)
			self.toolbar.EnableTool(3,False)
			self.toolbar.EnableTool(5,False)
			self.toolbar.EnableTool(4,False)

		dial.Destroy()
		
	def RefreshMods(self,event): 
		global Arma3Directory,ModListLeft,MissionsFolderChoices,EditorMissionsListBox
		sender=self.nb.GetSelection()
		if sender==0:
			if not Arma3Directory: 
				for objects in ModListLeft: 
					objects.Destroy() 
				del ModListLeft[:] 
				ModsListBox.Clear() 
			else:
				folderList=os.listdir(Arma3Directory)
		#-----Insert mod to the list  
				tempList=[] 
				for objects in ModListLeft: 
					tempList.append(objects.GetName()) 
				for modName in folderList: 
					if "@" in modName and os.path.isdir(Arma3Directory+"\\"+modName+"\\addons")==True: 
						if modName not in tempList: 
							ModListLeft.append(wx.CheckBox(self.page1, -1, modName, (20,25*len(ModListLeft)+20), (160,-1),name=modName)) 
		#-----Delete mod     
				templist2=[]
				counter=0
				for objects in ModListLeft:
					if objects.GetName() not in folderList:
						counter=1
				if counter==1:
					for objects in ModListLeft:
						objects.Destroy()
					del ModListLeft[:]
					for modName in folderList: 
						if "@" in modName and os.path.isdir(Arma3Directory+"\\"+modName+"\\addons")==True:
							ModListLeft.append(wx.CheckBox(self.page1, -1, modName, (20,25*len(ModListLeft)+20), (160,-1),name=modName)) 
		elif sender==2:
			if not MissionsDirectory:
				dlg = wx.MessageDialog(None, "Select mission's directory in settings first!", 'Oups!', wx.OK|wx.ICON_INFORMATION)
				dlg.ShowModal()
				dlg.Destroy()
			else:
				folderlist=os.listdir(MissionsDirectory)
				for maps in folderlist:
					if not maps.endswith(".Altis") or maps.endswith(".Stratis"):
						folderlist.remove(maps)
				if len(folderlist)>len(MissionsFolderChoices):
					templist=set(folderlist)-set(MissionsFolderChoices)
					for missions in list(templist):
						MissionsFolderChoices.append(missions)

				elif len(folderlist)<len(MissionsFolderChoices):
					templist=set(MissionsFolderChoices)-set(folderlist)
					for missions in list(templist):
						MissionsFolderChoices.remove(missions)
				EditorMissionsListBox.Set(MissionsFolderChoices)

	def SaveSettings(self,event):
		global priorityList,parameters
		selection=ProfileBox.GetSelection()
		profile=profiles[selection]
		settings=[parameters,priorityList]

		if not os.path.exists(App_Dir+"\\Profiles"):
			os.makedirs('Profiles')
			with open("Profiles/"+profile+".txt",'w') as fob:
				json.dump(settings,fob)
		else:
			try:
				os.chmod("Profiles/"+profile+".txt",stat.S_IWRITE)
				with open("Profiles/"+profile+".txt",'w') as fob:
					json.dump(settings,fob)
			except:
				try:
					os.makedirs('Profiles')
					with open("Profiles/"+profile+".txt",'w') as fob:
						json.dump(settings,fob)
				except:
					with open("Profiles/"+profile+".txt",'w') as fob:
						json.dump(settings,fob)
		os.chmod("Profiles/"+profile+".txt",stat.S_IREAD)	

	def CreateProfile(self,event):
		global parameters,priorityList,profiles
		profile=self.NewProfileText.GetValue()
		settings=[parameters,priorityList]
		if not os.path.exists(App_Dir+"\\Profiles"):
			os.makedirs('Profiles')
		if os.path.exists(App_Dir+"\\Profiles"+"\\"+profile+".txt"):
			dlg = wx.MessageDialog(self, "Profile '"+profile+"' already exists!", 'Oups!', wx.OK|wx.ICON_ERROR)
			dlg.ShowModal()
			dlg.Destroy()
		else:
			with open(App_Dir+"\\Profiles/"+profile+".txt",'w') as fob:
				json.dump(settings,fob)
			os.chmod(App_Dir+"\\Profiles/"+profile+".txt",stat.S_IREAD)
			self.NewProfileText.SetValue("")
			profiles.append(str(profile))
			ProfileBox.Set(profiles)
			ProfileBox.SetSelection(len(profiles)-1)
			self.toolbar.EnableTool(3,True)
			self.toolbar.EnableTool(5,True)
			self.toolbar.EnableTool(4,True)
	def OnSelectProfile(self,event):
		global parameters,priorityList,ModListLeft,CheckBoxesList,profiles,RamSliderTextCtrl,GpuSliderTextCtrl,selectAllButton
		if favouriteprofile==ProfileBox.GetStringSelection() and favouriteprofile in profiles:
			self.toolbar.EnableTool(4,False)
		else:
			self.toolbar.EnableTool(4,True)
		profile=ProfileBox.GetStringSelection()
		if not os.path.exists(App_Dir+"\\Profiles"+"\\"+profile+".txt"):
			dlg = wx.MessageDialog(self, "Profile '"+profile+"' does not exist!", 'Oups!', wx.OK|wx.ICON_ERROR)
			dlg.ShowModal()
			dlg.Destroy()
			profiles.remove(profile)
			ProfileBox.Set(profiles)
		else:
			for objects in CheckBoxesList[0:14]:
				objects.SetValue(False)
			for objects in CheckBoxesList[14:]:
				objects.Disable()

			with open(App_Dir+"\\Profiles\\"+profile+".txt") as fob:
				parameters.clear()
				del priorityList[:]
				settings=json.load(fob)
				parameters=settings[0]
				priorityList=settings[1]
			for objects in ModListLeft:
				if objects.GetName() in priorityList:
					objects.SetValue(True)
				else:
					objects.SetValue(False)
			for objects in CheckBoxesList[0:14]:
				if objects.GetName() in parameters:
					objects.SetValue(True)
			for i,j in parameters.items():
				if i=="-cpuCount=":
					CheckBoxesList[14].Enable()
					CheckBoxesList[14].SetSelection(cores.index(str(j)))
				elif i=="-maxMem=":
					CheckBoxesList[15].Enable()
					CheckBoxesList[15].SetValue(j)
					RamSliderTextCtrl.Enable()
					RamSliderTextCtrl.SetValue(str(j))
				elif i=="-maxVRAM=":
					CheckBoxesList[16].Enable()
					CheckBoxesList[16].SetValue(j)
					GpuSliderTextCtrl.Enable()
					GpuSliderTextCtrl.SetValue(str(j))
				elif i=="-exThreads=":
					CheckBoxesList[17].Enable()
					CheckBoxesList[17].SetSelection(exThreads.index(str(j)))
			ModsListBox.Set(priorityList)
			if len(ModListLeft)==len(priorityList):
				selectAllButton.SetValue(True)
				selectAllButton.SetLabel("Deselect All")
			else:
				selectAllButton.SetValue(False)
				selectAllButton.SetLabel("Select All")
			self.toolbar.EnableTool(3,True)
			self.toolbar.EnableTool(5,True)


#-------Menubar Functions----------------------------------------------------------------------

	def OnClose(self, event):
		self.tskic.Destroy()
		self.Destroy()
	def OnExit(self,event):
		self.Close(True)
	def OnFolders(self,event):
		global Arma3Directory
		dlg = wx.DirDialog(self, "Choose a directory:",style=wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST| wx.DD_CHANGE_DIR)
		if dlg.ShowModal() == wx.ID_OK:
			if "arma3.exe" not in os.listdir(dlg.GetPath()):
				dlg = wx.MessageDialog(self, 'No arma3.exe was found!', 'Oups!', wx.OK|wx.ICON_EXCLAMATION)
				dlg.ShowModal()
				dlg.Destroy()
			else:
				Arma3Directory=dlg.GetPath()
				try:
					os.chmod(App_Dir+"\\config.txt",stat.S_IWRITE)
					fob=open("config.txt",'r')
					temp=json.load(fob)
					fob.close()
					temp[0]=Arma3Directory
					fob=open("config.txt",'w')
					json.dump(temp,fob)
					fob.close()
				except:
					temp=[]
					temp.append(Arma3Directory)
					fob=open("config.txt",'w')
					json.dump(temp,fob)
					fob.close()
				os.chmod(App_Dir+"\\config.txt",stat.S_IREAD)
				LoadProfiles(self)
				ProfileBox.Set(profiles)
		dlg.Destroy()

	def OnMissions(self,event):
		global MissionsDirectory,MissionsFolderChoices,App_Dir
		dlg = wx.DirDialog(self, "Choose a directory:",style=wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST| wx.DD_CHANGE_DIR)
		if dlg.ShowModal() == wx.ID_OK:
			MissionsDirectory=dlg.GetPath()
			try:
				fob=open(App_Dir+"\\config.txt",'r')
				temp=json.load(fob)
				fob.close()
				os.chmod(App_Dir+"\\config.txt",stat.S_IWRITE)
				if len(temp)==0:
					temp.append("")
					temp.append(MissionsDirectory)
				elif len(temp)==1:
					temp.append(MissionsDirectory)
				else:
					temp[1]=MissionsDirectory
				fob=open(App_Dir+"\\config.txt",'w')
				json.dump(temp,fob)
				fob.close()
			except:
				temp=[]
				temp.append("")
				temp.append(MissionsDirectory)
				fob=open("config.txt",'w')
				json.dump(temp,fob)
				fob.close()
			os.chmod(App_Dir+"\\config.txt",stat.S_IREAD)
		dlg.Destroy()

	def OnAbout(self,event):
		description = "HACv Launcher is a simple launcher program for Arma 3 \nand was created for the community members of HACv."
		info = wx.AboutDialogInfo()
		info.SetIcon(wx.Icon(App_Dir+'\\images/hacv_logo.png', wx.BITMAP_TYPE_PNG))
		info.SetName('HACv Launcher')
		info.SetVersion('0.1')
		info.SetDescription(description)
		info.SetCopyright('(C) 2013 Vaggelis Theodoridis')
		info.SetWebSite('www.hac-virtual.org')
		wx.AboutBox(info)
		
#------------------------------------------------------------------------------------------------------------
#----------------------------------------------------TASKBAR-------------------------------------------------
#------------------------------------------------------------------------------------------------------------
class MyTaskBarIcon(wx.TaskBarIcon):
	def __init__(self, frame):
		wx.TaskBarIcon.__init__(self)

		self.frame = frame
		self.SetIcon(wx.Icon('images/TaskBar.ico', wx.BITMAP_TYPE_ICO), 'HACv Launcher')
		self.Bind(wx.EVT_MENU, self.OnTaskBarActivate, id=1)
		self.Bind(wx.EVT_MENU, self.OnTaskBarDeactivate, id=2)
		self.Bind(wx.EVT_MENU, self.OnTaskBarClose, id=3)
		self.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.OnTaskBarActivate)
	def CreatePopupMenu(self):
		menu = wx.Menu()
		menu.Append(1, 'Show')
		menu.Append(2, 'Hide')
		menu.AppendSeparator()
		menu.Append(3, 'Exit')
		return menu
	def OnTaskBarClose(self, event):
		self.frame.Close()
	def OnTaskBarActivate(self, event):
		if not self.frame.IsShown():
			self.frame.Show()

	def OnTaskBarDeactivate(self, event):
		if self.frame.IsShown():
			self.frame.Hide()		

class UpdateApp(wx.App,SoftwareUpdate):
	def OnInit(self):
		BASEURL = "http://platforma.comeze.com/HACv_Launcher"
		self.InitUpdates(BASEURL,BASEURL + "/"+'ChangeLog.txt')
		
		frame=MainFrame(parent=None,id=-1)
		frame.Show(True)
		self.SetTopWindow(frame)
		self.SetAppDisplayName('HACv_Launcher')
		return True 

if __name__ == '__main__':
	app=UpdateApp(redirect=False)
	app.MainLoop()
#--------------------------------------------INSPECTOOOOOOOOOOR-----Ctrl+Alt+I---------
# import wx.lib.mixins.inspection
# class MyApp(wx.App, wx.lib.mixins.inspection.InspectionMixin):
# 	def OnInit(self):
# 		self.Init()  # initialize the inspection tool
# 		frame = MainFrame(None, id=-1)
# 		frame.Show()
# 		self.SetTopWindow(frame)
# 		return True

# app = MyApp()
# app.MainLoop()
