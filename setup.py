import sys, os
import sys, os
from esky import bdist_esky
from setuptools import setup
from glob import glob
import version

if sys.platform == "win32":
    import py2exe
    
    FREEZER = 'py2exe'
    FREEZER_OPTIONS = dict(compressed = 0,
                           optimize = 0,
                           bundle_files = 3,
                           dll_excludes = ['MSVCP90.dll',
                                           'mswsock.dll',
                                           'powrprof.dll', 
                                           'USP10.dll',],
                        )


NAME = "HACvLauncher"
APP = [bdist_esky.Executable("GUI.py", 
                             gui_only=True,
                             )]
DATA_FILES = [ ("images", glob(r'.\images\*.*')) ]

ESKY_OPTIONS = dict( freezer_module     = FREEZER,
                     freezer_options    = FREEZER_OPTIONS,
                     enable_appdata_dir = True,
                     bundle_msvcrt      = True,
                     )
    

# Build the app and the esky bundle
setup( name       = NAME,
       scripts    = APP,
       version    = version.VERSION,
       data_files = DATA_FILES,
       options    = dict(bdist_esky=ESKY_OPTIONS)
       )
